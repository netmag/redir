/*
 *  Redir, utility applet for web
 *  Copyright (C) 1998 Radim Kolar <hsn@cybermail.net>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA or
 *  download copy from http://www.gnu.org/copyleft/gpl.html
 *
 */


/* Redir 1.0

 (c) Radim Kolar [hsn@cybermail.net] 1998
 
 http://ncic.netmag.cz/apps/nase/redir.html
 
 History:
 1.3.1998 - 1.0 version flyes to world (Someone from FidoNet
                 requested it)
 
*/

import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class redir extends java.applet.Applet
{

  public static final String COPY="REDIR 1.00 (C) Radim Kolar (hsn@cybermail.net) 1998.\nhttp://ncic.netmag.cz/apps/nase/redir.html";
  
  private Color bgcolor; 
  
  
  public String getAppletInfo()
  {
   return COPY;
  }    

  public void init()
  {
   String par;
   par=getParameter("bgcolor");
   if(par!=null)
                bgcolor=parseColorString(par);
              else
                bgcolor=Color.white;
                
  setBackground(bgcolor);

  }
  
  public void start()
  {
   /* a jedeeeeeem! */
   String target;
   String def;
   target=getParameter("target");
   def=getParameter("default");
   
   if(target==null) target="_self";
   
   /* smycka */
   /* location1 -> kam poslat browser */
   /* property1 -> podle jake polozky, nic->posle vzdy */
   /* value1    -> co se ma v ni hledat, nic->pouze pokud existuje */
  
   String location,property,value,prop;
   int i=0;
   while(true)
   {
    i++;
    location=property=value=null;
    location=getParameter("location"+i);
    if(location==null) break;
    property=getParameter("property"+i);
    if(property==null) { redirTo(location,target);return;}
    value=getParameter("value"+i);
    /* Testujeme polozku! */
    
     try
     {
      prop=System.getProperty(property);
     }
     catch(SecurityException e) { continue;}
     
     if(prop==null) continue;
     if(value==null) { redirTo(location,target);return;}
     /* case dolu */
     value=value.toLowerCase();
     prop=prop.toLowerCase();
     
     if(prop.indexOf(value)>-1) { redirTo(location,target);return;}

   }
   redirTo(def,target);
  }
    
  private void redirTo(String URL, String target)
  {
   if(URL==null) return;
   URL loc;
   try{
     loc=new URL(URL);
      }
   catch(MalformedURLException e) { return;}
   System.out.println("Redirecting to: "+URL);
   getAppletContext().showDocument(loc,target);
  }
  
  private Color parseColorString(String colorString)
    {


        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }
 
     
}